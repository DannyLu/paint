#include <TGUI/TGUI.hpp>
#include "Rectangle.h"
#include <vector>


int main(){
    std::vector < sf::RectangleShape > recdrawings;

    sf::VideoMode resolution = sf::VideoMode().getDesktopMode();
    sf::RenderWindow window(resolution, "Paint", sf::Style::Default);
    //window.setVerticalSyncEnabled(true);
    //sf::RectangleShape background(sf::Vector2f(1280, 720));
    tgui::Gui gui{window};

    tgui::Button::Ptr button = tgui::Button::create();
    button->setPosition(40,25);
    button->setText("Clear");
    button->setSize(100, 40);
    button->connect("pressed", [&](){ recdrawings.clear(); });
    gui.add(button);


    Rectangle *rect = new Rectangle(30, sf::Color::Green);
    sf::RectangleShape brush = rect->getRectangle();

    while(window.isOpen()) {
        sf::Event e;
        while (window.pollEvent(e)) {
            if (e.type == sf::Event::Closed) {
                window.close();
            }

            gui.handleEvent(e);

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
                // Left click draws current brush
                sf::Vector2i mouse = sf::Mouse::getPosition(window);
                brush.setPosition((float)mouse.x, (float)mouse.y);
                recdrawings.push_back(brush);
            }
            else if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
                // Set right click to "erase"
                brush.setFillColor(sf::Color::White);
                sf::Vector2i mouse = sf::Mouse::getPosition(window);
                brush.setPosition((float)mouse.x, (float)mouse.y);
            }

        }


        window.clear(sf::Color::White);
        gui.draw();
        // Draw vector of shapes
        for (int i = 0; i < recdrawings.size(); i++) {
            window.draw(recdrawings[i]);
        }
        window.display();

    }

    return 0;
}
